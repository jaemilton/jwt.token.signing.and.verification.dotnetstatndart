﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace jwt.signiture
{
    public class ExpiredCertificateException : Exception
    {
        public ExpiredCertificateException(string message): base(message) { } 
    }
}
