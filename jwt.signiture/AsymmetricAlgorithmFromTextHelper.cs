﻿using Jose;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace jwt.signiture
{
    /// <summary>
    /// Helper class to get an AsymmetricAlgorithm instance object from 
    /// a certificate, public key or private key text that starts with: 
    ///     -----BEGIN PUBLIC KEY-----
    ///     -----BEGIN CERTIFICATE-----
    ///     -----BEGIN RSA PUBLIC KEY-----
    ///     -----BEGIN RSA PRIVATE KEY-----
    ///     -----BEGIN PRIVATE KEY-----
    ///     -----BEGIN ENCRYPTED PRIVATE KEY-----
    /// 
    /// The AsymmetricAlgorithmObject that can be used as key to
    ///     Jose.JWT.Encode
    ///     Jose.JWT.Decode
    ///     
    /// Example of reading privatekey from https://vcsjones.dev/2019/10/07/key-formats-dotnet-3/
    /// </summary>
    public class AsymmetricAlgorithmFromTextHelper
    {
        private const string PublicKeyHeader = "-----BEGIN PUBLIC KEY-----";
        private const string PublicKeyFooter = "-----END PUBLIC KEY-----";
        private const string CertificateHeader = "-----BEGIN CERTIFICATE-----";
        private const string CertificateFooter = "-----END CERTIFICATE-----";
        private const string RSAPublicKeyHeader = "-----BEGIN RSA PUBLIC KEY-----";
        private const string RSAPublicKeyFooter = "-----END RSA PUBLIC KEY-----";

        private const string RSAPrivateKeyHeader = "-----BEGIN RSA PRIVATE KEY-----";
        private const string RSAPrivateKeyFooter = "-----END RSA PRIVATE KEY-----";
        private const string PrivateKeyHeader = "-----BEGIN PRIVATE KEY-----";
        private const string PrivateKeyFooter = "-----END PRIVATE KEY-----";
        private const string PrivateEncryptedKeyHeader = "-----BEGIN ENCRYPTED PRIVATE KEY-----";
        private const string PrivateEncryptedKeyFooter = "-----END ENCRYPTED PRIVATE KEY-----";
        private readonly string _passphase = null;

        private string _stringBase64 = null;

        /// <summary>
        /// Contructor to identify witch type of certificate, public key or private key is being used
        /// </summary>
        /// <param name="privateKeyOrCetificateText"></param>
        /// <param name="passphase"></param>
        public AsymmetricAlgorithmFromTextHelper(string privateKeyOrCetificateText, string passphase = null)
        {
            if (string.IsNullOrEmpty(privateKeyOrCetificateText))
            {
                throw new ArgumentException("privateKeyOrCetificateText can't be null");
            }

            PrivateKeyOrPublicKeyOrCetificateText = privateKeyOrCetificateText.Trim();

            if (CheckHeaderAndFooter(PublicKeyHeader, PublicKeyFooter))
            {
                HeaderLabel = PublicKeyHeader;
                FooterLabel = PublicKeyFooter;
                AsymmetricAlgorithmType = AsymmetricAlgorithmTypeEnum.PublicKey;
            }
            else if (CheckHeaderAndFooter(CertificateHeader, CertificateFooter))
            {
                HeaderLabel = CertificateHeader;
                FooterLabel = CertificateFooter;
                AsymmetricAlgorithmType = AsymmetricAlgorithmTypeEnum.Certificate;
            }
            else if (CheckHeaderAndFooter(RSAPublicKeyHeader, RSAPublicKeyFooter))
            {
                HeaderLabel = RSAPublicKeyHeader;
                FooterLabel = RSAPublicKeyFooter;
                AsymmetricAlgorithmType = AsymmetricAlgorithmTypeEnum.RSAPublicKey;
            }
            else if (CheckHeaderAndFooter(RSAPrivateKeyHeader, RSAPrivateKeyFooter))
            {
                HeaderLabel = RSAPrivateKeyHeader;
                FooterLabel = RSAPrivateKeyFooter;
                AsymmetricAlgorithmType = AsymmetricAlgorithmTypeEnum.RSAPrivateKey;
            }
            else if (CheckHeaderAndFooter(PrivateKeyHeader, PrivateKeyFooter))
            {
                HeaderLabel = PrivateKeyHeader;
                FooterLabel = PrivateKeyFooter;
                AsymmetricAlgorithmType = AsymmetricAlgorithmTypeEnum.Pkcs8PrivateKey;
            }
            else if (CheckHeaderAndFooter(PrivateEncryptedKeyHeader, PrivateEncryptedKeyFooter))
            {
                HeaderLabel = PrivateEncryptedKeyHeader;
                FooterLabel = PrivateEncryptedKeyFooter;
                AsymmetricAlgorithmType = AsymmetricAlgorithmTypeEnum.EncryptedPkcs8PrivateKey;
                if (string.IsNullOrEmpty(passphase))
                {
                    throw new ArgumentException("privateKeyOrCetificateText can't be null");
                }

                _passphase = passphase;
            }
            else
            {
                throw new InvalidAlgorithmException("Impossible to identify the Algorithm");
            }
        }

        /// <summary>
        /// Helper method to check if the a certificate, public key or private key starts and ends with 
        /// specified header and footer
        /// </summary>
        /// <param name="header"></param>
        /// <param name="footer"></param>
        /// <returns></returns>
        private bool CheckHeaderAndFooter(string header, string footer )
        {
            return (PrivateKeyOrPublicKeyOrCetificateText.StartsWith(header) && 
                PrivateKeyOrPublicKeyOrCetificateText.EndsWith(footer));
        }

        /// <summary>
        /// Text of Certificate ou Private key Or Public Key received on constructor
        /// </summary>
        public string PrivateKeyOrPublicKeyOrCetificateText { get; } = null;

        /// <summary>
        /// Header string of  Certificate ou Private key Or Public Key received on constructor
        /// </summary>
        private string HeaderLabel { get; } = null;

        /// <summary>
        /// Footer string of Certificate ou Private key Or Public Key received on constructor
        /// </summary>
        private string FooterLabel { get; } = null;

        /// <summary>
        /// Base64 string of Certificate ou Private key Or Public Key received on constructor
        /// </summary>
        private string StringBase64
        {
            get
            {
                if (string.IsNullOrEmpty(_stringBase64))
                {
                    _stringBase64 = PrivateKeyOrPublicKeyOrCetificateText.Replace(HeaderLabel, string.Empty)
                            .Replace(FooterLabel, string.Empty)
                            .Replace(Environment.NewLine, string.Empty);
                }

                return _stringBase64;
            } 
        }

        /// <summary>
        /// Byte Array of Certificate ou Private key Or Public Key received on constructor (Without Header and Footer)
        /// </summary>
        private byte[] SourceBytes
        {
            get
            {
                return Convert.FromBase64String(StringBase64);
            }
        }

        /// <summary>
        /// Enum to identify the type of text passed on contructor
        /// </summary>
        public AsymmetricAlgorithmTypeEnum AsymmetricAlgorithmType { get; }

        /// <summary>
        /// Instance of AsymmetricAlgorithm created from text passed on contructor to be user to encode or decode
        /// jwt messages
        /// </summary>
        public AsymmetricAlgorithm AsymmetricAlgorithmObject
        {
            get
            {
                AsymmetricAlgorithm asymmetricAlgorithm = null;
                switch (AsymmetricAlgorithmType)
                {
                    case AsymmetricAlgorithmTypeEnum.RSAPrivateKey:
                        asymmetricAlgorithm = RSA.Create();
                        ((RSA)asymmetricAlgorithm).ImportRSAPrivateKey(SourceBytes, out _);
                        break;
                    case AsymmetricAlgorithmTypeEnum.Pkcs8PrivateKey:
                        asymmetricAlgorithm = RSA.Create();
                        ((RSA)asymmetricAlgorithm).ImportPkcs8PrivateKey(SourceBytes, out _);
                        break;
                    case AsymmetricAlgorithmTypeEnum.EncryptedPkcs8PrivateKey:
                        asymmetricAlgorithm = RSA.Create();
                        ((RSA)asymmetricAlgorithm).ImportEncryptedPkcs8PrivateKey(new ReadOnlySpan<char>(_passphase.ToCharArray()), SourceBytes,  out _);
                        break;
                    case AsymmetricAlgorithmTypeEnum.RSAPublicKey:
                        asymmetricAlgorithm = RSA.Create();
                        ((RSA)asymmetricAlgorithm).ImportRSAPublicKey(SourceBytes, out _);
                        break;                        
                    case AsymmetricAlgorithmTypeEnum.PublicKey:
                        asymmetricAlgorithm = RSA.Create();
                        ((RSA)asymmetricAlgorithm).ImportSubjectPublicKeyInfo(SourceBytes, out _);
                        break;
                    case AsymmetricAlgorithmTypeEnum.Certificate:
                        var certificate = new X509Certificate2(SourceBytes);
                        if (certificate.NotAfter.CompareTo(DateTime.Now) >= 0)
                        {
                            asymmetricAlgorithm = certificate.PublicKey.Key;
                        }
                        else
                        {
                            throw new ExpiredCertificateException($"The certificate expired in {certificate.NotAfter}");
                        }
                        
                        break;
                }

                return asymmetricAlgorithm;
            }
        }

    }
}
