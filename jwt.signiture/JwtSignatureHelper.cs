﻿using Jose;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace jwt.signiture
{

    public static class JwtSignatureHelper
    {

        /// <summary>
        /// Creates a signed JWT with a private key certificate
        /// </summary>
        /// <param name="jsonJwtPayload"></param>
        /// <param name="privateKeyText">
        /// Expects the PEM file content
        /// Is is prepared to receive 
        /// To summarize each PEM labels accepted to sign a Json:
        ///     “BEGIN RSA PRIVATE KEY”
        ///     “BEGIN PRIVATE KEY”
        ///     “BEGIN ENCRYPTED PRIVATE KEY”
        /// </param>
        /// <param name="jwsAlgorithm"></param>
        /// <param name="jwtExtraHeader"></param>
        /// <param name="passphase"></param>
        /// <returns>Returns a signed jwt token</returns>
        public static string CreateSignedJwt
        (
            object objectPayload,
            string privateKeyText,
            JwsAlgorithm jwsAlgorithm,
            IDictionary<string, object> jwtExtraHeader = null,
            string passphase = null
        )
        {
            var asymmetricAlgorithmFromTextHelper =
                new AsymmetricAlgorithmFromTextHelper
                (
                    privateKeyText,
                    passphase
                );

            switch (asymmetricAlgorithmFromTextHelper.AsymmetricAlgorithmType)
            {
                case AsymmetricAlgorithmTypeEnum.RSAPrivateKey:
                case AsymmetricAlgorithmTypeEnum.Pkcs8PrivateKey:
                case AsymmetricAlgorithmTypeEnum.EncryptedPkcs8PrivateKey:
                    using (var rsa = asymmetricAlgorithmFromTextHelper.AsymmetricAlgorithmObject)
                    {
                        // Example: use the PrivateKey from the certificate above for signing a JWT token using Jose.Jwt:
                        return Jose.JWT.Encode(objectPayload, rsa, jwsAlgorithm, jwtExtraHeader);
                    }
                case AsymmetricAlgorithmTypeEnum.RSAPublicKey:
                case AsymmetricAlgorithmTypeEnum.PublicKey:
                case AsymmetricAlgorithmTypeEnum.Certificate:
                default:
                    throw new InvalidAlgorithmException("Only private keys are accepted to Sign a Json");
            }

        }

        /// <summary>
        /// Verify a JWT token ao ensure that it`s signed currecty against a public key 
        /// and returns payload json text of jwt token
        /// 
        /// Expects the PEM file content
        /// Is is prepared to receive 
        /// To summarize each PEM labels:
        ///     “BEGIN RSA PRIVATE KEY”
        ///     “BEGIN PRIVATE KEY”
        ///     “BEGIN ENCRYPTED PRIVATE KEY” 
        ///     “BEGIN RSA PUBLIC KEY”
        ///     “BEGIN PUBLIC KEY” 
        ///     “BEGIN CERTIFICATE” 
        /// </summary>
        /// <param name="encodedJwtToken"></param>
        /// <param name="publicKeyOrCertificateOrPrivateKey"></param>
        /// <param name="jwsAlgorithm"></param>
        /// <param name="passphase"></param>
        /// <returns></returns>
        public static string GetJsonFromSignedJwt
        (
            string encodedJwtToken, 
            string publicKeyOrCertificateOrPrivateKey, 
            JwsAlgorithm jwsAlgorithm, 
            string passphase = null
        )
        {
            var asymmetricAlgorithmFromTextHelper =
                new AsymmetricAlgorithmFromTextHelper(publicKeyOrCertificateOrPrivateKey, passphase);

            using (var rsa = asymmetricAlgorithmFromTextHelper.AsymmetricAlgorithmObject)
            {
                return Jose.JWT.Decode(encodedJwtToken, rsa, jwsAlgorithm);
            }
        }



        
        /// <summary>
        /// Verify a JWT token ao ensure that it`s signed currecty against a public key 
        /// and returns payload json text of jwt token
        /// 
        /// Expects the PEM file content
        /// Is is prepared to receive 
        /// To summarize each PEM labels:
        ///     “BEGIN RSA PRIVATE KEY”
        ///     “BEGIN PRIVATE KEY”
        ///     “BEGIN ENCRYPTED PRIVATE KEY” 
        ///     “BEGIN RSA PUBLIC KEY”
        ///     “BEGIN PUBLIC KEY” 
        ///     “BEGIN CERTIFICATE” 
        /// </summary>
        /// <typeparam name="T">Type of object that will be deserialized from json payload</typeparam>
        /// <param name="encodedJwtToken"></param>
        /// <param name="publicKeyOrCertificateOrPrivateKey"></param>
        /// <param name="jwsAlgorithm"></param>
        /// <param name="passphase"></param>
        /// <returns></returns>
        public static T GetJsonFromSignedJwt<T>
        (
            string encodedJwtToken,
            string publicKeyOrCertificateOrPrivateKey,
            JwsAlgorithm jwsAlgorithm,
            string passphase = null
        )
        {
            var asymmetricAlgorithmFromTextHelper =
                new AsymmetricAlgorithmFromTextHelper(publicKeyOrCertificateOrPrivateKey, passphase);

            using (var rsa = asymmetricAlgorithmFromTextHelper.AsymmetricAlgorithmObject)
            {
                return Jose.JWT.Decode<T>(encodedJwtToken, rsa, jwsAlgorithm);
            }
        }

    }
}
