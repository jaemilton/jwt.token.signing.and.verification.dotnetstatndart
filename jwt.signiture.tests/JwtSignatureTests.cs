using Jose;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace jwt.signiture.tests
{

    internal class JsonObjectSend
    {
        [JsonProperty("sub")]
        public string Subject { get; set; }

        [JsonProperty("exp")]
        public int Exp { get; set; }

        public override bool Equals(object obj)
        {
            JsonObjectSend objToCompre = (JsonObjectSend)obj;

            return (objToCompre.Subject == this.Subject && objToCompre.Exp == this.Exp);
        }
    }

    public class JwtSignatureTests
    {
        private const string nonEncryptedPrivateKey = @"-----BEGIN PRIVATE KEY-----
                                                MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQD15TNB5Ms2zKSQ
                                                97U0qTIZrRU9fRnCuscCUkeP3VNJp4LyISjsMdvPeyMSNaiOWxPSqG/3Sc9n6R9m
                                                4btgxB3R4LKg7iBHCi8XctrXJajHA7yhSgK3ukpAG1oGfQhgDw5oPgF98kizIG3b
                                                Py4IaRJKE8wMO0R9+hGH8P0gC48fUnzS2BF+tGXDENzIiVUYfHDAQmK26AsxCyyR
                                                XGjXG/GXzTnt4nm6ZXwRj2cERdo2D3DT0/9mMAAGzRwJlooV7YcTSYRODfSKkrrH
                                                EN3NEv5EOxtcBOys2tf25KjSDG3k7F4ByaI3WiogaKfK92NcjvOP4g6rzBYnVV4e
                                                orucN/+PAgMBAAECggEBAKFa5pi9xu/Nt8t9EIZe5IZewopX1hinMcH/NgBcjrPN
                                                XSr/wXfEe+YZenkPhuzrLQXWC6rs31Hjcvyy8Z9c8ehwFydtOvcC4sXzU1FM4ATA
                                                Gf6Ir2Hat7B7gkaxs1Y9awnSbm40F6qbhBsTyWQxdpPvi70XAynbd7eJ1sA5dZBp
                                                6kCW4sezspbgic73/YtiRdNmPfaNQjV3YpJi51Fc6qoZgGZMkxOLLfqlAt7f6h59
                                                VAkTyB3KxmIvFSNqCMu6QR8yXSm6a3MTmqNqW6sHVw4rXv2PGZpPGJjUa1cqKtF1
                                                obROUrAau+gMUiQREXwc8L4QKb6TdAkQQCuoOBgHlFkCgYEA/Bhwk0zqn7UXyXg7
                                                hHzW1KD/iyoOrjTvxJD5Kp+VyWoN0wN+2/WrH4a8vDauQfO1NUhSsNm90AgMmxbw
                                                0BhoGd80ZL/0aOTQdGP8aUBHZPVPeltO3m/MuugQs2xC+hvDWpAzfqZvYoMQt6ht
                                                b1TG0aQOoBjBWJXTmHe/8G9gGysCgYEA+bQtRDXzyWiV7XNtOYed6WEj6kFOiJWv
                                                h4pxLKF4dBUsbKxRyRGC9i9btyHLtibXiSxF+uiN5xRep9o0Q9WtyA7sGbPzPPp0
                                                d7e9l3Fmhp0MX9HwgthrQGNholWzDtcqGhIV6m24vxXndXUXTJI6t/9DKT5kWzB8
                                                jlhTFS3JKy0CgYBpixnmsbcxMjJJSSsx5spSry0N6IfI66TOmRuYesPNVbd/9uCv
                                                2oTQaCqwafw+5HejXSoxnJfZduVAMTSVCwLaghZ/b9Y17EZR1GKi9iRt4j5qLbwx
                                                aIFAZ1RGqjbIW8NIMUM97RSJG/Jt0fgvOkEbGCJ/aJ3aJrA2DzyuvxJvjQKBgHsL
                                                Q/sGsvieZrK5XCsrHq8vxOjcAXzgdbxOhfI+JwLcpCVwzGROEsCJGaKmLqG1hiP8
                                                AwferH4f1XzU00hR9O5OHB2WRNfZeqa7jdsgcCsJHF3jL6bqSf9SeXvqyu8XK5Hp
                                                M49vPcg7XPDOIAsFS5X7XPbn3b/pppbmAxq+kxCJAoGBAOJVXZ7tzNaOwrG0UwSc
                                                cpjtV1bG7cMXyHdBaHPFgzXFuFd0bKRgnubButMi6E02q3SyfYPNinH3RoQ+UD1M
                                                6P6WT+Y7Fgxj6+FRePsCsTR3whwSwCU7ws6YVPV7Lpf8c1hWXeFCgf5yFPa3fGTo
                                                6lrIkFtiTIm3tIEEtl/OaSJm
                                                -----END PRIVATE KEY-----";

        private const string encryptedPrivateKey = @"-----BEGIN ENCRYPTED PRIVATE KEY-----
                                                MIIFLTBXBgkqhkiG9w0BBQ0wSjApBgkqhkiG9w0BBQwwHAQIA3SsaGa5xr4CAggA
                                                MAwGCCqGSIb3DQIJBQAwHQYJYIZIAWUDBAEqBBBppNmOxycCynkbZo0XFlKlBIIE
                                                0OG7bi81iib6nDee83JssebWcw2LqQLUS3gIAcoYs0ft3WR//0JoiH1xwh71GlVL
                                                CZMLixR44iJXqE0klCwnIXUbZs2lb+sr+qrVVxlFfuJ3GlwYCsxpk3UqJjRMp0hq
                                                CPLdMm8APkJXHMhUY4sindUHiczBO4bU3qG2iizHbvoyTIpEeGcDtdRIza/XRAVw
                                                Pu0852r1i37mpaztouMVZs2aZKiyMa0jChg4u4dekfBmN8HZgMX/qx5G6+N60V92
                                                1byOCZQHErsgjWPW2vIx56K+41ozsBY2+DuDh01G+SwUPlks/4UudcVjRmuDKddW
                                                TJy+JE6REXEKug6/U+wRToWWpAk2GW5SUuNpEOiceaB8HsRmc4Msm80m3GOjmtd5
                                                yJTj9W6Iqdyn6dW9xzCqDAQLK04hoAinHW56ganpRWy6wMqYQDQ/Zj0Hl1eH9ghv
                                                b0dYjpaX/SOHPpP19gdMS8Cg0xVh9P6dFg5AoMj+ahM2kAbkiB8k9+UXeM4sOWPp
                                                qoiBZT2OnYaAgZBSGzGYzE3G25u3NjmznFzxjIWOKqga+Vsh4Go5bzyuhMlackNY
                                                f5PWo5dtO9ZGoDK6aJZK8P/3Mt8tA5VtrlO4+zOGa7mMK/PrScOyFOdaaArGfEdi
                                                jnZ+QeNH6fXomgvzDpZ/HNhSS97ePvLJowXPSP/+42BLbDl/dzTCJDcPDyMTglOL
                                                yzYCQ50edv/MY/a8B4DAS91R0NIoNnreOAfhiVys1BEkwTzWfbh0JOo6pwAykFtW
                                                R8pUNoHG3k2sm7zF6kUvvlhx2Vo2rPcr6Saa80zT6zIg3KBu1LFaZ09Qg9I3pUZN
                                                R6qMBT5YG3VCKhvJ+AOWS13z20eT+mR6u7Ge1wTwR7+duJsqtuyoiXVVFw58Xk7F
                                                vcMSyE09iXe9PUc1ptOMqoF7pvmhuhaGc9g6jO061iEN/qLJPW8IjX/frfosr9Vr
                                                3TiRoGqvO486I1Gjtw0qMocZNwpRir2mWTraUSli3M/WQhO1bjTf5IwzwO+jBivw
                                                VbGA5Y5Xv2dcvtw6DVgHhK3Z8Ip0UR/2fbk73udqeiUlL2uRITR9Ppt1ITBoieuy
                                                Wjpu2sIJV/VPeJUlVzEYMmfDKqffMmCAMii0myrwU+P6NFeDPXZIqlbZBtu30cQ7
                                                T9Z1aPm83+fCC7ocyUIoViDlghnJaot+z7X2/QklHK8ijz4OadXeH6gO9Xizmyg8
                                                755XelgcYyd8rzG6eIxeBnFALyqaghMz0Lblp6fT4+Nu37sWoEGk3vvLpyg7rG+u
                                                n/sdB7OQ9mlZ6MqoBAu91JFzJgvTHJN7bSIelTVzVSitR77ofQsq+/955YzGHgqA
                                                l7muuqtIlsxhC6shuHkQQ9eq8L8uTrZPYngg0BEcMsFyt0iR86vndectNqHhHrJq
                                                cmT5qD82aIYOF0UvmfzsbPJZgIi2m19cyrMiEXYvaCtLW5D8FKoBuvolFStaadgp
                                                6L/mqWvpCSXhpc0S70fuosLkEYj0XGw9y5iTq3zGZf7JTxsQjMrUCaHOnzVL6FuG
                                                7//oS4zzrzFwglivy2PCKdKCDpM2QAh6+e1SlsB/g0srbDQJaut6lJNQgmQLuIQZ
                                                TOdVdSV7Gy2bRSMSX/mn9M3PunecMRwCKwCN44W4PJ/M
                                                -----END ENCRYPTED PRIVATE KEY-----";
        private const string encryptionPassphase = "123456";

        private const string certificateText = @"-----BEGIN CERTIFICATE-----
                                    MIID8jCCA1ugAwIBAgIBBzANBgkqhkiG9w0BAQsFADBvMRkwFwYDVQQDExBwZnNl
                                    bnNlLWJlbGVtLWNhMQswCQYDVQQGEwJCUjESMBAGA1UECBQJU+NvIFBhdWxvMRIw
                                    EAYDVQQHFAlT428gUGF1bG8xDTALBgNVBAoTBENhc2ExDjAMBgNVBAsTBUJlbGVt
                                    MB4XDTIwMDYyNjEzMzU1M1oXDTIyMDYyNjEzMzU1M1owaDETMBEGA1UEAwwKdGVz
                                    dGUuY2VydDENMAsGA1UECwwEQ2FzYTENMAsGA1UECgwEQ2FzYTESMBAGA1UEBwwJ
                                    U2FvIFBhdWxvMRIwEAYDVQQIDAlTYW8gUGF1bG8xCzAJBgNVBAYTAkJSMIIBIjAN
                                    BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9eUzQeTLNsykkPe1NKkyGa0VPX0Z
                                    wrrHAlJHj91TSaeC8iEo7DHbz3sjEjWojlsT0qhv90nPZ+kfZuG7YMQd0eCyoO4g
                                    RwovF3La1yWoxwO8oUoCt7pKQBtaBn0IYA8OaD4BffJIsyBt2z8uCGkSShPMDDtE
                                    ffoRh/D9IAuPH1J80tgRfrRlwxDcyIlVGHxwwEJitugLMQsskVxo1xvxl8057eJ5
                                    umV8EY9nBEXaNg9w09P/ZjAABs0cCZaKFe2HE0mETg30ipK6xxDdzRL+RDsbXATs
                                    rNrX9uSo0gxt5OxeAcmiN1oqIGinyvdjXI7zj+IOq8wWJ1VeHqK7nDf/jwIDAQAB
                                    o4IBHzCCARswCQYDVR0TBAIwADALBgNVHQ8EBAMCBeAwMQYJYIZIAYb4QgENBCQW
                                    Ik9wZW5TU0wgR2VuZXJhdGVkIFVzZXIgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFBvD
                                    uZacmsP8McI5nCIQ47zyMXThMIGZBgNVHSMEgZEwgY6AFAA8Obp58INkChgQy141
                                    /3ZL1ftRoXOkcTBvMRkwFwYDVQQDExBwZnNlbnNlLWJlbGVtLWNhMQswCQYDVQQG
                                    EwJCUjESMBAGA1UECBQJU+NvIFBhdWxvMRIwEAYDVQQHFAlT428gUGF1bG8xDTAL
                                    BgNVBAoTBENhc2ExDjAMBgNVBAsTBUJlbGVtggEAMBMGA1UdJQQMMAoGCCsGAQUF
                                    BwMCMA0GCSqGSIb3DQEBCwUAA4GBAK+bEIxUii27SgQGjyCbXEvE86yYewZbJiBN
                                    AhxefkgSaAjYIuNhhmfaVaDhlNYKtzS0VS3wmicc6aWUoN+on0MHVkqriQRIyWID
                                    6Gvsqxbf4YVCQUuTrTnAs4zEF+qOqw9ld3dXGXMwvCJji4DdNUcBDreI2+GPee83
                                    elyRuxMy
                                    -----END CERTIFICATE-----";

        private const string rsaPrivateKey = @"-----BEGIN RSA PRIVATE KEY-----
                                                MIIEjAIBAAKB/QK63dMKRSZlsv2pRMkuZjRgV4K4IooA2v4Hbzt+Yf09Q740+Yl6
                                                ZfI7yXAfcnxNsZ0pLiNyvGFmXLxgbCZYUMp3FOkB9G2H8txt+AGWN2+PnwCE9uFn
                                                M1sjABGXWiLRB3os/nU51nVo4vYLMiSQ7JvBHmaznLHmgGVapS2Q8UjDFn7DyMBO
                                                BbniU+IKf2mnELz0476PSv5c2UiVERltAx935+4ifnmpGWRSQjzqZZVw3aLF47yc
                                                lRf2rD25qmVkiNu1X0GpSHz2rVpORRgJ0WSyYubEwAdfm5UrTC+g6zu9A1FVc48l
                                                J7EA7cX3Hsys+PSdLxJBNrCWQgbT6yECAwEAAQKB/QISzoMJbOYyHlYd57nqPQSR
                                                HEaT9t/csHh7Sxfj8Sc4Dij88X9i8OWgVWqM6BbTqBDXg0y3S6+q4MrPbbVeJNby
                                                xXYI3w8GUAbum40Q/EAG1Ny/IQioqtAQ8G9sBrElFMnOPOLqsCHzLVd1v3sYfC38
                                                60tUJrLUgTb6NzU5vpdhiGNDOH/F43MDPhdukJR+LBKfFn/i2VJfaHmJSNJK3WwB
                                                7c54gQKt0YFB7TBAMbaD190700aSQYAdHcTwudCN9kU8goxIbJzZJWK7XpNnX56N
                                                dU16/Yo8+W9cr1/ybYPZELu/z4yc3RrFooj2Qd1k9QWUV6JuA4Ax9YuLBAUCfwHM
                                                T8An4GERH0+3xXMkxAr58reVUxWGjA/x8PfSWOKF3cYV+qJcui7a3pu8U+GC2Whc
                                                +2QUVUohj4EJXVh1lJ2xUSkSnjsmmrxVzkHgxap53c3G8EiU1JqgjdJT6J3biohv
                                                UcULikqYIsoxhodZa5tyYfu1JqLG98p+D72g5L8CfwGEq9KE82tJKjGpkNofz6Co
                                                AJV48eHsbYCGU0SuXKwfUtwSs3DnJvlVASbBeqvAKRqh2kSO4VA0jY0u3vbiE88T
                                                gSnkLa0bFHXtsLjg2399qHD5h6Mt8dOaN3FgaeBlsjbsd+1vniCVjC7s/9PMW4sC
                                                khq0MjkWbpKgnowdyB8CfwC65n7NMiv3edEyevd/rCIQzt5/wRZyZRhsUj0tqW2A
                                                kUXG0tdt8yLnN4/gbeaY7JGVf3VpVCAfdFceaEpso4VL4/pbVugJiraB2JejHl9T
                                                0B2ncxTlaVXHbw5Bh6fg5mTzXi41u1cCYs3aQhde1XRugx5itWdOhnovqVCDrZ0C
                                                fwD3SV7yyfELpvXb/dZaAP8lhOGVBsiIZY8tZ5M+IKY+c7ijiVmfKDPguo/O796Q
                                                r1ZLfhS6l24xUZJvLNsFMjdBIJ/nlowZu6Xgadzm66Cs8VuX142h8MeByh6vig2E
                                                hqPdRdxidMgkfgFpNsepsOC77PtihMW/mPBfkyBw3l8Cfh9f4N2eZ6B91tOpA+Pm
                                                TkCtSrFja8xiuHILMRxkD4yQeHi7WIOih258+iofWM7+8wR3c77ilBQ7CDrD4Zkr
                                                blxCU9wlazQBPDhijjEYsrDjQAcC/dy/uTnUxN/mk/K7/395kL3zgOLQXQhKZz10
                                                nn3NeiHN7w4w7imKl9u+Ww==
                                                -----END RSA PRIVATE KEY-----";

        private const string rsaPublicKey = @"-----BEGIN RSA PUBLIC KEY-----
                                            MIIBBQKB/QK63dMKRSZlsv2pRMkuZjRgV4K4IooA2v4Hbzt+Yf09Q740+Yl6ZfI7
                                            yXAfcnxNsZ0pLiNyvGFmXLxgbCZYUMp3FOkB9G2H8txt+AGWN2+PnwCE9uFnM1sj
                                            ABGXWiLRB3os/nU51nVo4vYLMiSQ7JvBHmaznLHmgGVapS2Q8UjDFn7DyMBOBbni
                                            U+IKf2mnELz0476PSv5c2UiVERltAx935+4ifnmpGWRSQjzqZZVw3aLF47yclRf2
                                            rD25qmVkiNu1X0GpSHz2rVpORRgJ0WSyYubEwAdfm5UrTC+g6zu9A1FVc48lJ7EA
                                            7cX3Hsys+PSdLxJBNrCWQgbT6yECAwEAAQ==
                                            -----END RSA PUBLIC KEY-----";

        private const string publicKey = @"-----BEGIN PUBLIC KEY-----
                                            MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9eUzQeTLNsykkPe1NKky
                                            Ga0VPX0ZwrrHAlJHj91TSaeC8iEo7DHbz3sjEjWojlsT0qhv90nPZ+kfZuG7YMQd
                                            0eCyoO4gRwovF3La1yWoxwO8oUoCt7pKQBtaBn0IYA8OaD4BffJIsyBt2z8uCGkS
                                            ShPMDDtEffoRh/D9IAuPH1J80tgRfrRlwxDcyIlVGHxwwEJitugLMQsskVxo1xvx
                                            l8057eJ5umV8EY9nBEXaNg9w09P/ZjAABs0cCZaKFe2HE0mETg30ipK6xxDdzRL+
                                            RDsbXATsrNrX9uSo0gxt5OxeAcmiN1oqIGinyvdjXI7zj+IOq8wWJ1VeHqK7nDf/
                                            jwIDAQAB
                                            -----END PUBLIC KEY-----";

        private const string invalidPublicKey = @"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9eUzQeTLNsykkPe1NKky
                                                Ga0VPX0ZwrrHAlJHj91TSaeC8iEo7DHbz3sjEjWojlsT0qhv90nPZ+kfZuG7YMQd
                                                0eCyoO4gRwovF3La1yWoxwO8oUoCt7pKQBtaBn0IYA8OaD4BffJIsyBt2z8uCGkS
                                                ShPMDDtEffoRh/D9IAuPH1J80tgRfrRlwxDcyIlVGHxwwEJitugLMQsskVxo1xvx
                                                l8057eJ5umV8EY9nBEXaNg9w09P/ZjAABs0cCZaKFe2HE0mETg30ipK6xxDdzRL+
                                                RDsbXATsrNrX9uSo0gxt5OxeAcmiN1oqIGinyvdjXI7zj+IOq8wWJ1VeHqK7nDf/
                                                jwIDAQAB";

        private const string privateKeyFromExpiredCertificate  = @"-----BEGIN PRIVATE KEY-----
                                                        MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDa6RZM3w9mNlEn
                                                        9CxqYZ4Lbj5v0XqQfyIyUkl/zB6Grk2UYOWjGafIR3XMuTHRmbnBs7Gd8clIpTew
                                                        SepDl2xe/F8Mjh8vtntlA7jgbJc5AnMBqLigKPwBbop/CDyoeWk+23yi4GE0FITE
                                                        c6nRtOTWR4Z7MvOtRIVhB/OKCZZ6E/rqXLRdHBXmoPkIdAR+6a8dEZ5XSDsMOkR7
                                                        /RpfXbqQSuPO4O6ocRWS92ND9HtlPNiAUa0tHLdNGWvApUKngnt4xX1ZQSlE1y99
                                                        qJKrPAgtNGgD/xw2hT7bS3zSndclE94MC290SqRHPRvSdV1AZ7Hq7kZFQep6URJj
                                                        0kEZ/0P3AgMBAAECggEBAMZSNpbL2pqiCOImbvqtl04JwVzyLZSs6OEn9VhnJxyi
                                                        2rvbEQ+zFXL3cnxXJxAZsdUPo5FQ1CcRLyNxw18a1t4yyzWHqC9EpgO83IfNOS83
                                                        zdMp9KF8n9n4OFafsrx6yeNdW0v/9XAGSsXjd2z58ftC7027ulE165QdbQCYcRAE
                                                        TuLl7KiWH8Lv/Js8w4V4/pJjEh959LXQpyN7d+WfUvBg1UhAQzpO75rF83EVfyJf
                                                        xbUTEwXcu8bBly8W59OT/CN49DLQT1rFuaG/9CqOKPwLy6Ji4RhHmqrI/PUIzx+j
                                                        rPpUN2FfUyITdldWgDhe+BXc+K3xwp79hmTqVTWGeNECgYEA8Q3C0+AAEH6pLZBf
                                                        ZLdsXoNcTgQif6ysCFx9mYkCOJwvEMh9rwRA/kG4dGbV+aIaE/ifA+qe3+jrymLk
                                                        fVFtVTuvHMWq6xUmStYsYhOkbPBt5aLxTQsXnm3w4JNeK+4zf0ES91BFsmrYV4vs
                                                        1KS7QvN3wTTGU4Fm2KcylOM6RjkCgYEA6HvYyFYnal6Db682hiHRYniLRJg0IMKY
                                                        JurahaqDz6tah/u8Qf0hIB5s6qsU9bR/dxD4oImSRrR6bCKS/3k07Ha/aUQA0cNp
                                                        CnYx/V3eqC9DcEX9yOmjDbBa9fCAGSTm2AVyAmpHtsV0/WtvClF46AlJknCRUYHY
                                                        lsPBqVfLW68CgYAUKdyEMJaQyqeIIi14lUdBWKed9waTu3Pgdywiba174sSlGMA6
                                                        pZZyLjkps3humzqnNQLIJxYHEMq4cTE/slH88TMiqv4zD9/WHrgztATq/+mRbeHV
                                                        WfotzHB7uJgGALTaDi1x9or986YJHLFA0E7BuO/pf2qfjZF2yfWXMrKrMQKBgF1N
                                                        hv0wBnWcJOfny9wdKml4s3v+Eb2sG2BTFQxcGLZ5sMWudCmPq4G2dEPiEXFi1V01
                                                        xHK5g6k4uYE4hwJc5K6aIcvfbOzCnh+CvCFRURDylwAyNt4xVi0iP/9AXEi7FKJR
                                                        3SW2/b0Olhs2yMO9WubVA1/wKYIquIAZZFnQUF8lAoGBAPAdtg4Whto4YaWLn0sd
                                                        VRcmRE3oO1PiSq+kANIuwK3rEexgenS1tmyb+ovTU4WyO+kBDXam0tQLcgZEqN4d
                                                        FyVSL58OwiFZtSBHQdrPFZJ4dy1WlpA7GVzB8voJNI9TRqxchFjt6jztrmmtbgAc
                                                        KIbEQ8QE1sb3g1vQTaBHp/Tm
                                                        -----END PRIVATE KEY-----";

        //Certificate expired on 2020-06-27
        private const string expiredCertificateText = @"-----BEGIN CERTIFICATE-----
                                                        MIID+zCCA2SgAwIBAgIBBjANBgkqhkiG9w0BAQsFADBvMRkwFwYDVQQDExBwZnNl
                                                        bnNlLWJlbGVtLWNhMQswCQYDVQQGEwJCUjESMBAGA1UECBQJU+NvIFBhdWxvMRIw
                                                        EAYDVQQHFAlT428gUGF1bG8xDTALBgNVBAoTBENhc2ExDjAMBgNVBAsTBUJlbGVt
                                                        MB4XDTIwMDYyNjEzMzEzNloXDTIwMDYyNzEzMzEzNlowcTEcMBoGA1UEAwwTdGVz
                                                        dGUuY2VydC5leHBpcmFkbzENMAsGA1UECwwEQ2FzYTENMAsGA1UECgwEQ2FzYTES
                                                        MBAGA1UEBwwJU2FvIFBhdWxvMRIwEAYDVQQIDAlTYW8gUGF1bG8xCzAJBgNVBAYT
                                                        AkJSMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2ukWTN8PZjZRJ/Qs
                                                        amGeC24+b9F6kH8iMlJJf8wehq5NlGDloxmnyEd1zLkx0Zm5wbOxnfHJSKU3sEnq
                                                        Q5dsXvxfDI4fL7Z7ZQO44GyXOQJzAai4oCj8AW6Kfwg8qHlpPtt8ouBhNBSExHOp
                                                        0bTk1keGezLzrUSFYQfzigmWehP66ly0XRwV5qD5CHQEfumvHRGeV0g7DDpEe/0a
                                                        X126kErjzuDuqHEVkvdjQ/R7ZTzYgFGtLRy3TRlrwKVCp4J7eMV9WUEpRNcvfaiS
                                                        qzwILTRoA/8cNoU+20t80p3XJRPeDAtvdEqkRz0b0nVdQGex6u5GRUHqelESY9JB
                                                        Gf9D9wIDAQABo4IBHzCCARswCQYDVR0TBAIwADALBgNVHQ8EBAMCBeAwMQYJYIZI
                                                        AYb4QgENBCQWIk9wZW5TU0wgR2VuZXJhdGVkIFVzZXIgQ2VydGlmaWNhdGUwHQYD
                                                        VR0OBBYEFM2S52oN/GCx6SwGwWSIQ2z4McnpMIGZBgNVHSMEgZEwgY6AFAA8Obp5
                                                        8INkChgQy141/3ZL1ftRoXOkcTBvMRkwFwYDVQQDExBwZnNlbnNlLWJlbGVtLWNh
                                                        MQswCQYDVQQGEwJCUjESMBAGA1UECBQJU+NvIFBhdWxvMRIwEAYDVQQHFAlT428g
                                                        UGF1bG8xDTALBgNVBAoTBENhc2ExDjAMBgNVBAsTBUJlbGVtggEAMBMGA1UdJQQM
                                                        MAoGCCsGAQUFBwMCMA0GCSqGSIb3DQEBCwUAA4GBAEMqp4aEGkB6t5+uNv7MmNA2
                                                        2Pyi7jiseX/52VNMGmcSQGSpc48anstjPdsKtfvuIXbbUirDL/LAqyNCbjKZWPiC
                                                        UuFHk6ex5PK5+zA0ljx+znCFZX83NKrndLlVQya126xN+0ACiRXmPdOcew0Vz1EM
                                                        hcf+n0EXCzi9bOQcJpw0
                                                        -----END CERTIFICATE-----";

        [SetUp]
        public void Setup()
        {
        }


        #region "Tests with certificate and private key"
        #region "Tests with JWT WITHOUT extra header" 


        [Test]
        public void SigningJwtWithNoExtraHeaderAndWithEncryptedPrivateKeyTest()
        {
            var payload = new Dictionary<string, object>()
            {
                { "sub", "mr.x@contoso.com" },
                { "exp", 1300819380 }
            };

            string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, encryptedPrivateKey, Jose.JwsAlgorithm.RS256, null, encryptionPassphase);

            string expectedSignedJwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";
            Assert.AreEqual(expectedSignedJwt, signedJwt);
        }


        [Test]
        public void SigningJwtFromObjectTest()
        {

            var payload = new JsonObjectSend
            {
                Subject = "mr.x@contoso.com",
                Exp = 1300819380
            };

            string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, nonEncryptedPrivateKey, Jose.JwsAlgorithm.RS256, null, encryptionPassphase);

            string expectedSignedJwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";
            Assert.AreEqual(expectedSignedJwt, signedJwt);
        }

        [Test]
        public void SignedTokenVerificationFromObjectWithValidSignatureTest()
        {
            string encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";

            var expectedJsonObject = new JsonObjectSend
            {
                Subject = "mr.x@contoso.com",
                Exp = 1300819380
            };
            JsonObjectSend jsonObject = JwtSignatureHelper.GetJsonFromSignedJwt<JsonObjectSend>(encodedToken, certificateText, Jose.JwsAlgorithm.RS256);

            Assert.AreEqual(expectedJsonObject, jsonObject);
        }




        [Test]
        public void SigningJwtWithNoExtraHeaderAndWithEncryptedPrivateKeyWithoutPassingPassphaseTest()
        {
            Assert.Throws<ArgumentException>
            (
                () =>
                {
                    var payload = new Dictionary<string, object>()
                    {
                        { "sub", "mr.x@contoso.com" },
                        { "exp", 1300819380 }
                    };

                    string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, encryptedPrivateKey, Jose.JwsAlgorithm.RS256);
                }
            );
        }

        [Test]
        public void SigningJwtWithNoExtraHeaderAndWithNonEncryptedPrivateKeyTest()
        {
            var payload = new Dictionary<string, object>()
            {
                { "sub", "mr.x@contoso.com" },
                { "exp", 1300819380 }
            };

            string expectedSignedJwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";
            string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, nonEncryptedPrivateKey, Jose.JwsAlgorithm.RS256, null, "123456");

            Assert.AreEqual(expectedSignedJwt, signedJwt);
        }

        [Test]
        public void SignedTokenVerificationWithValidSignatureTest()
        {
            string encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";

            string expectedJson = "{\"sub\":\"mr.x@contoso.com\",\"exp\":1300819380}";
            string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, certificateText, Jose.JwsAlgorithm.RS256);

            Assert.AreEqual(expectedJson, json);
        }

        [Test]
        public void SignedTokenVerificationWithInvalidSignatureTest()
        {
            Assert.Throws<Jose.IntegrityException>
                (() =>
                {
                    string encodedTokenWithInvalidSignature = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7rxoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";
                    string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedTokenWithInvalidSignature, certificateText, Jose.JwsAlgorithm.RS256);
                }
            );
        }

        #endregion "Tests with JWT WITHOUT extra header" 

        #region "Tests with JWT WITH extra header" 
        [Test]
        public void SigningJwtWithExtraHeaderAndWithEncryptedPrivateKeyTest()
        {
            var extraJwtHeader = new Dictionary<string, object>()
            {
                { "id", 1234567890 },
                { "date", "01/01/2020" }
            };

            var payload = new Dictionary<string, object>()
            {
                { "sub", "mr.x@contoso.com" },
                { "exp", 1300819380 }
            };

            string expectedSignedJwt = "eyJhbGciOiJSUzI1NiIsImlkIjoxMjM0NTY3ODkwLCJkYXRlIjoiMDEvMDEvMjAyMCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.H0R291AwU8rWmCVKrkwBCa9L1oQqVuOLhTH_0fbmnSY5wEIeSArim2Td9Q-S06yIo-ftpLw2jv1VOHygAwzThNvoMycDyfnliQUIl0Qd8F5x58VoRKd_sE3vTAlwH_h_H-LfiJPY3aAYFo3iVAMwUrQeqYo90rNm7bxailBd0rI8EFy0XbKFWJeLixpCfc1TpYSZOVtzu2wGnWxJ7isC8PFPhHg8PDqO8jyH7KH5HUAYCZI4TnILDzbSRfztWvl-cHYDVIKEEbZHCDozplA74yu7ozksDOWoWmWMsM88IDDdOGgHTF3DSyUvbN4zkGmGO4p8RNbZEtNvl-CTQgxWPg";
            string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, encryptedPrivateKey, Jose.JwsAlgorithm.RS256, extraJwtHeader, "123456");

            Assert.AreEqual(expectedSignedJwt, signedJwt);
        }

        [Test]
        public void SigningJwtWithExtraHeaderAndWithNonEncryptedPrivateKeyTest()
        {
            var extraJwtHeader = new Dictionary<string, object>()
            {
                { "id", 1234567890 },
                { "date", "01/01/2020" }
            };

            var payload = new Dictionary<string, object>()
            {
                { "sub", "mr.x@contoso.com" },
                { "exp", 1300819380 }
            };

            string expectedSignedJwt = "eyJhbGciOiJSUzI1NiIsImlkIjoxMjM0NTY3ODkwLCJkYXRlIjoiMDEvMDEvMjAyMCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.H0R291AwU8rWmCVKrkwBCa9L1oQqVuOLhTH_0fbmnSY5wEIeSArim2Td9Q-S06yIo-ftpLw2jv1VOHygAwzThNvoMycDyfnliQUIl0Qd8F5x58VoRKd_sE3vTAlwH_h_H-LfiJPY3aAYFo3iVAMwUrQeqYo90rNm7bxailBd0rI8EFy0XbKFWJeLixpCfc1TpYSZOVtzu2wGnWxJ7isC8PFPhHg8PDqO8jyH7KH5HUAYCZI4TnILDzbSRfztWvl-cHYDVIKEEbZHCDozplA74yu7ozksDOWoWmWMsM88IDDdOGgHTF3DSyUvbN4zkGmGO4p8RNbZEtNvl-CTQgxWPg";
            string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, nonEncryptedPrivateKey, Jose.JwsAlgorithm.RS256, extraJwtHeader, "123456");

            Assert.AreEqual(expectedSignedJwt, signedJwt);
        }


        [Test]
        public void SignedTokenWithExtraHeaderVerificationWithValidSignatureTest()
        {
            string encodedToken = "eyJhbGciOiJSUzI1NiIsImlkIjoxMjM0NTY3ODkwLCJkYXRlIjoiMDEvMDEvMjAyMCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.H0R291AwU8rWmCVKrkwBCa9L1oQqVuOLhTH_0fbmnSY5wEIeSArim2Td9Q-S06yIo-ftpLw2jv1VOHygAwzThNvoMycDyfnliQUIl0Qd8F5x58VoRKd_sE3vTAlwH_h_H-LfiJPY3aAYFo3iVAMwUrQeqYo90rNm7bxailBd0rI8EFy0XbKFWJeLixpCfc1TpYSZOVtzu2wGnWxJ7isC8PFPhHg8PDqO8jyH7KH5HUAYCZI4TnILDzbSRfztWvl-cHYDVIKEEbZHCDozplA74yu7ozksDOWoWmWMsM88IDDdOGgHTF3DSyUvbN4zkGmGO4p8RNbZEtNvl-CTQgxWPg";

            string expectedJson = "{\"sub\":\"mr.x@contoso.com\",\"exp\":1300819380}";
            string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, certificateText, Jose.JwsAlgorithm.RS256);

            Assert.AreEqual(expectedJson, json);
        }

        #endregion "Tests with JWT WITH extra header" 
        #endregion "Tests with certificate and private key"

        #region "Tests with RSA private and public keys"
        [Test]
        public void SigningJwtWithRsaPrivateKeyTest()
        {
            var payload = new Dictionary<string, object>()
            {
                { "sub", "mr.x@contoso.com" },
                { "exp", 1300819380 }
            };

            string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, rsaPrivateKey, Jose.JwsAlgorithm.RS256);

            string expectedSignedJwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.Ag-2cVe7oTytKEQcTcsMXRbR2grVyCxdgXzRy2-UjrRmlkN69sWH-J7dgSG9FX1udr_aivvYr_iQdcK8rjcCm8nDF7I7MLEBMzzZJ-EAnN7CkSZa2fGNMUUp4RzyzCkog4f6cgvswsuAJip7I-1xjEF0CMK7eOoPaN3z74DlFMH6V-UMnawgSyFhGGBJefneQU7G03BnlvLFVsCVapdraRgGvJLjIyt6F2G4UvUmt9xuDtRQJ3oWV28nkLYziqA7q0QMSIVyTOblUusj41kuMWBGLmFrXpyxJwHHTt-OK5Rd2qhVUiqGwyYvd2Qq-449mtwbOvTTHPRheDv5UA";
            Assert.AreEqual(expectedSignedJwt, signedJwt);
        }

        [Test]
        public void SignedTokenVerificationWithRsaPublicKeyTest()
        {
            string encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.Ag-2cVe7oTytKEQcTcsMXRbR2grVyCxdgXzRy2-UjrRmlkN69sWH-J7dgSG9FX1udr_aivvYr_iQdcK8rjcCm8nDF7I7MLEBMzzZJ-EAnN7CkSZa2fGNMUUp4RzyzCkog4f6cgvswsuAJip7I-1xjEF0CMK7eOoPaN3z74DlFMH6V-UMnawgSyFhGGBJefneQU7G03BnlvLFVsCVapdraRgGvJLjIyt6F2G4UvUmt9xuDtRQJ3oWV28nkLYziqA7q0QMSIVyTOblUusj41kuMWBGLmFrXpyxJwHHTt-OK5Rd2qhVUiqGwyYvd2Qq-449mtwbOvTTHPRheDv5UA";

            string expectedJson = "{\"sub\":\"mr.x@contoso.com\",\"exp\":1300819380}";
            string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, rsaPublicKey, Jose.JwsAlgorithm.RS256);

            Assert.AreEqual(expectedJson, json);
        }
        #endregion  "Tests with RSA private and public keys"

        #region "Tests with private and public keys"

        [Test]
        public void SignedTokenVerificationWithPublicKeyTest()
        {
            string encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";

            string expectedJson = "{\"sub\":\"mr.x@contoso.com\",\"exp\":1300819380}";
            string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, publicKey, Jose.JwsAlgorithm.RS256);

            Assert.AreEqual(expectedJson, json);
        }

        #endregion "Tests with private and public keys"

        #region "Tests With expired certificate"
        [Test]
        public void SigningJwtWithNonEncryptedPrivateKeyFromExpiredCertificateTest()
        {
            var payload = new Dictionary<string, object>()
            {
                { "sub", "mr.x@contoso.com" },
                { "exp", 1300819380 }
            };

            string expectedSignedJwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.S6Woo-OEjyhX37U-ub22IqUeEVahP1S4i4IyScKrlIapskF5gDiC6xVVVERC2xdzklnm20ZOp48le6soEH9kC3pxgasbZX2ok7Y6UJA8KhxoCyOJF6g6JuOEvps2G5ghWpSg6Ib2IQUIPiFli-6EG7hHPCFSM5ZX05k694_IyeeaGLVeCfx0nwi0UwmYm3M5d3_O2bISdNBR18OhTSOPhewkP5MUAoWr4d13MmbuQK_tExgJHVvso-h9Yp5wlf_YFEz5Yx47SrfvoP_sqmYpPDHBNnDJk7ZWk1e7-SY7ZhxbpVqA0R2EDIbkdVrstl35a6v2u1uCq2mfPIzi5qjlYQ";
            string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, privateKeyFromExpiredCertificate, Jose.JwsAlgorithm.RS256, null, "123456");

            Assert.AreEqual(expectedSignedJwt, signedJwt);
        }

        [Test]
        public void SigningJwtWithExpiredCertificateTest()
        {
            Assert.Throws<ExpiredCertificateException>(
                () =>
                {
                    string encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.S6Woo-OEjyhX37U-ub22IqUeEVahP1S4i4IyScKrlIapskF5gDiC6xVVVERC2xdzklnm20ZOp48le6soEH9kC3pxgasbZX2ok7Y6UJA8KhxoCyOJF6g6JuOEvps2G5ghWpSg6Ib2IQUIPiFli-6EG7hHPCFSM5ZX05k694_IyeeaGLVeCfx0nwi0UwmYm3M5d3_O2bISdNBR18OhTSOPhewkP5MUAoWr4d13MmbuQK_tExgJHVvso-h9Yp5wlf_YFEz5Yx47SrfvoP_sqmYpPDHBNnDJk7ZWk1e7-SY7ZhxbpVqA0R2EDIbkdVrstl35a6v2u1uCq2mfPIzi5qjlYQ";

                    string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, expiredCertificateText, Jose.JwsAlgorithm.RS256);
                }
            );

            
            
        }

        #endregion "Tests With expired certificate"

        #region "Tests using Invalid Keys, NoKey or Public Key to Sign"

        [Test]
        public void SigningJwtWithCertificateOrPublicKeyTest()
        {

            Assert.Throws<InvalidAlgorithmException>(
                () =>
                {
                    var payload = new Dictionary<string, object>()
                    {
                        { "sub", "mr.x@contoso.com" },
                        { "exp", 1300819380 }
                    };

                    string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, rsaPublicKey, Jose.JwsAlgorithm.RS256, null, "123456");
                 }
            );

            Assert.Throws<InvalidAlgorithmException>(
               () =>
               {
                   var payload = new Dictionary<string, object>()
                   {
                        { "sub", "mr.x@contoso.com" },
                        { "exp", 1300819380 }
                   };

                   string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, publicKey, Jose.JwsAlgorithm.RS256, null, "123456");
               }
           );

            Assert.Throws<InvalidAlgorithmException>(
               () =>
               {
                   var payload = new Dictionary<string, object>()
                   {
                        { "sub", "mr.x@contoso.com" },
                        { "exp", 1300819380 }
                   };

                   string signedJwt = JwtSignatureHelper.CreateSignedJwt(payload, certificateText, Jose.JwsAlgorithm.RS256, null, "123456");
               }
           );

        }

        [Test]
        public void SignedTokenVerificationWithPrivateKeyTest()
        {
            string expectedJson = "{\"sub\":\"mr.x@contoso.com\",\"exp\":1300819380}";
            
            string encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";
     
            //Verifing with encryptedPrivateKey
            string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, encryptedPrivateKey, Jose.JwsAlgorithm.RS256, encryptionPassphase);
            Assert.AreEqual(expectedJson, json);

            //Verifing with nonEncryptedPrivateKey
            json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, nonEncryptedPrivateKey, Jose.JwsAlgorithm.RS256);
            Assert.AreEqual(expectedJson, json);

            //Verifing with RsaPrivateKey
            encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.Ag-2cVe7oTytKEQcTcsMXRbR2grVyCxdgXzRy2-UjrRmlkN69sWH-J7dgSG9FX1udr_aivvYr_iQdcK8rjcCm8nDF7I7MLEBMzzZJ-EAnN7CkSZa2fGNMUUp4RzyzCkog4f6cgvswsuAJip7I-1xjEF0CMK7eOoPaN3z74DlFMH6V-UMnawgSyFhGGBJefneQU7G03BnlvLFVsCVapdraRgGvJLjIyt6F2G4UvUmt9xuDtRQJ3oWV28nkLYziqA7q0QMSIVyTOblUusj41kuMWBGLmFrXpyxJwHHTt-OK5Rd2qhVUiqGwyYvd2Qq-449mtwbOvTTHPRheDv5UA";
            json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, rsaPrivateKey, Jose.JwsAlgorithm.RS256);
            Assert.AreEqual(expectedJson, json);

        }

        [Test]
        public void SignedTokenVerificationWithInvalidPublicKeyTest()
        {
            Assert.Throws<InvalidAlgorithmException>
            (
                () =>
                {
                    string encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";
                    string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, invalidPublicKey, Jose.JwsAlgorithm.RS256);
                }
            );

        }

        [Test]
        public void SignedTokenVerificationWithoutPublicKeyTest()
        {
            Assert.Throws<ArgumentException>
            (
                () =>
                {
                    string encodedToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtci54QGNvbnRvc28uY29tIiwiZXhwIjoxMzAwODE5MzgwfQ.vSKzSpILoO6Qpq_Iy1q3g6fkYaWGEyw1LeoJOmDk2KhuV82QOW0Cwrusmaqh6m1FcvVq2sn7uze8PmP53VWV7wJbWUUb7WKeKm-rfl92bq2bmDyFu-Yd88el2elRAd6hPEbc5D-sgAS2a_2FiCMzCJ5thoiKxqeYVPV5BA8z4pZ4rHpcAVAo2o6A4V9tZMhD4yZkJUBSdbBcxYS1CNShhed1z-HiipjMfYC2TyFoIYaJssKi_GZSqX-bnwAj2YTl_An_c_qfssHccQeMQp8zVBTLCJT7raoxj4A3kED9xuagBZZyE3DWSCsNOMu8UKDiHiRP5O7xnvc4A6jH1hWkJw";
                    string json = JwtSignatureHelper.GetJsonFromSignedJwt(encodedToken, null, Jose.JwsAlgorithm.RS256);
                }
            );

        }

        #endregion "Invalid Keys and NoKey"



    }
}