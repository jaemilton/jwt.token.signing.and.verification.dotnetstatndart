Examples of how to generate keys using openssl

How to generate RSA Keys 
	- Private (-----BEGIN RSA PRIVATE KEY-----)
	- Public (-----BEGIN RSA PUBLIC KEY-----)

	1- Generate a RSA private keys (-----BEGIN RSA PRIVATE KEY-----)
		
		command: openssl genrsa -out mykey.pem 2048
![](.\generating_rsa_private_key.png)

	It will generate a RSA private key like this
		-----BEGIN RSA PRIVATE KEY-----
		MIIEjAIBAAKB/QK63dMKRSZlsv2pRMkuZjRgV4K4IooA2v4Hbzt+Yf09Q740+Yl6
		ZfI7yXAfcnxNsZ0pLiNyvGFmXLxgbCZYUMp3FOkB9G2H8txt+AGWN2+PnwCE9uFn
		M1sjABGXWiLRB3os/nU51nVo4vYLMiSQ7JvBHmaznLHmgGVapS2Q8UjDFn7DyMBO
		BbniU+IKf2mnELz0476PSv5c2UiVERltAx935+4ifnmpGWRSQjzqZZVw3aLF47yc
		lRf2rD25qmVkiNu1X0GpSHz2rVpORRgJ0WSyYubEwAdfm5UrTC+g6zu9A1FVc48l
		J7EA7cX3Hsys+PSdLxJBNrCWQgbT6yECAwEAAQKB/QISzoMJbOYyHlYd57nqPQSR
		HEaT9t/csHh7Sxfj8Sc4Dij88X9i8OWgVWqM6BbTqBDXg0y3S6+q4MrPbbVeJNby
		xXYI3w8GUAbum40Q/EAG1Ny/IQioqtAQ8G9sBrElFMnOPOLqsCHzLVd1v3sYfC38
		60tUJrLUgTb6NzU5vpdhiGNDOH/F43MDPhdukJR+LBKfFn/i2VJfaHmJSNJK3WwB
		7c54gQKt0YFB7TBAMbaD190700aSQYAdHcTwudCN9kU8goxIbJzZJWK7XpNnX56N
		dU16/Yo8+W9cr1/ybYPZELu/z4yc3RrFooj2Qd1k9QWUV6JuA4Ax9YuLBAUCfwHM
		T8An4GERH0+3xXMkxAr58reVUxWGjA/x8PfSWOKF3cYV+qJcui7a3pu8U+GC2Whc
		+2QUVUohj4EJXVh1lJ2xUSkSnjsmmrxVzkHgxap53c3G8EiU1JqgjdJT6J3biohv
		UcULikqYIsoxhodZa5tyYfu1JqLG98p+D72g5L8CfwGEq9KE82tJKjGpkNofz6Co
		AJV48eHsbYCGU0SuXKwfUtwSs3DnJvlVASbBeqvAKRqh2kSO4VA0jY0u3vbiE88T
		gSnkLa0bFHXtsLjg2399qHD5h6Mt8dOaN3FgaeBlsjbsd+1vniCVjC7s/9PMW4sC
		khq0MjkWbpKgnowdyB8CfwC65n7NMiv3edEyevd/rCIQzt5/wRZyZRhsUj0tqW2A
		kUXG0tdt8yLnN4/gbeaY7JGVf3VpVCAfdFceaEpso4VL4/pbVugJiraB2JejHl9T
		0B2ncxTlaVXHbw5Bh6fg5mTzXi41u1cCYs3aQhde1XRugx5itWdOhnovqVCDrZ0C
		fwD3SV7yyfELpvXb/dZaAP8lhOGVBsiIZY8tZ5M+IKY+c7ijiVmfKDPguo/O796Q
		r1ZLfhS6l24xUZJvLNsFMjdBIJ/nlowZu6Xgadzm66Cs8VuX142h8MeByh6vig2E
		hqPdRdxidMgkfgFpNsepsOC77PtihMW/mPBfkyBw3l8Cfh9f4N2eZ6B91tOpA+Pm
		TkCtSrFja8xiuHILMRxkD4yQeHi7WIOih258+iofWM7+8wR3c77ilBQ7CDrD4Zkr
		blxCU9wlazQBPDhijjEYsrDjQAcC/dy/uTnUxN/mk/K7/395kL3zgOLQXQhKZz10
		nn3NeiHN7w4w7imKl9u+Ww==
		-----END RSA PRIVATE KEY-----

	2 - Generate a RSA public keys (-----BEGIN RSA PUBLIC KEY-----) from a RSA private key

		command: openssl rsa -in .\rsa.private.key.pem -RSAPublicKey_out -out pubkey.pem
![](.\generating_rsa_public_key.png)

	It will generate a RSA private key like this
		-----BEGIN RSA PUBLIC KEY-----
		MIIBBQKB/QK63dMKRSZlsv2pRMkuZjRgV4K4IooA2v4Hbzt+Yf09Q740+Yl6ZfI7
		yXAfcnxNsZ0pLiNyvGFmXLxgbCZYUMp3FOkB9G2H8txt+AGWN2+PnwCE9uFnM1sj
		ABGXWiLRB3os/nU51nVo4vYLMiSQ7JvBHmaznLHmgGVapS2Q8UjDFn7DyMBOBbni
		U+IKf2mnELz0476PSv5c2UiVERltAx935+4ifnmpGWRSQjzqZZVw3aLF47yclRf2
		rD25qmVkiNu1X0GpSHz2rVpORRgJ0WSyYubEwAdfm5UrTC+g6zu9A1FVc48lJ7EA
		7cX3Hsys+PSdLxJBNrCWQgbT6yECAwEAAQ==
		-----END RSA PUBLIC KEY-----



2- Generate a 2048 bit RSA Public key: (-----BEGIN PRIVATE KEY-----) 

command:  openssl genpkey -algorithm RSA -out key.pem -pkeyopt rsa_keygen_bits:2048 
![](.\generating_private_key.png)

	It will generate a private key like this
	-----BEGIN PRIVATE KEY-----
	MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDYZLZFepTPc3r7
	0e3yMkqAbZ78FY6WfQKiLex1Upm6y0h2M4No3qCx4dLoSQms7MwK66PzzV6UaM/n
	qpLAhWmbiSZiX1U0AA3UGq1iMHnmkz0vMSrGgOYMujAHOJ8SXLAfN4JwH2cP/1yC
	3Cu7aMGuQBKVQb4RD8mjwWhU80V8ymKXP92xIZFd+eYoYBNGUkFwSNaqfLfHbz15
	GGGgWsdgElvaKz/GMM7YFOy7iZ7YtJx7+I/aF30Zv/PxL4cQSLBqQUy5UKfcymVJ
	sEQ7leuVGTyN6zKqeJLvKjEIuGvzMqOJhORpdVGpJO/pvmDlrKCBtNoGkR7cy7uy
	Be/GlCtnAgMBAAECggEBAI77vRSbssiA3bqXNErcwC2sQWkANX9qitruzVeYhMvQ
	lsjDsQkSUcw4E0w5NpN0xD5TBbqK8PvPZdEF/IIJpYaIUTyfEYWSo5CLMBW5QlMA
	52TPcBxJ/7hZLnMXDsTrSdTWsPG1Tjxb150ALkVfKtsgqybEkfpkHDCAE5hORpP1
	T7F8dkvj+R8YwC22OGjMmVLHHdyb6rvBVngnYo5GWXunlT9662baCQyapEOg44Q8
	+kHh2cxEyZ30zmUJVm3cWdhZeVh6N6bUGL8s4WahaynD8R+q/5pclR3mRQsAl75a
	EgLfEq5hfaMRuDNq7u/TpbZkfUM+aXWTy0rk+3K8oIECgYEA7ZYjLiEP7GC58H0o
	4EE2YoFoE19YuGE5k01/8ltBuu4bUtJMITBU0F22i6creGK90FuVW4/zeSj9x2mq
	c8WBqcscj4rNJM28P4kGXGKFJ4me9/O6wOwdDThXHchokxY3yPy6FL7I3waytHyx
	QfMz5KR/skxK7631993b1ZlQKJkCgYEA6SoXS608Uij8PVPft6MKK6WeH1ofRa3u
	DJRQ6SjDfvpyp8DiCN3/Ro+5OZmEFbBGKjM7vVuBet4WPHTXKA8HfRyL4E8sKPVa
	SufubjxFTF+SIlcmYAYr8rYMPFd/7a25EPSkeHjxhRlwy0M5qFGQTPLa5cE0X8by
	g6i/Ur3Xc/8CgYEAwO8/OmKW77nMxUKeQx4x7+Rug+zVu0UY4En7MPByfD6H6cXl
	FL6O4XZ4mwfyAR0OT3lP3L8VVLq4+4sPZFxrY1sRom9xEAYSbPX1OGi5ANZ9RZWj
	fq8/GFE8TW9FnMxaGNlCsz4LGTHzRSZNO+UnqvD60euqnuCB8fs0XkzWD2ECgYEA
	lHuYgDne6xdsooVyN5idVnROwPp8ZbxAl0QwTnsGHCvTrh64ExuEgGPweczYf5NU
	63Ta+1h1mVAq6u8hQUJo63StYvgoUCEHEiKnBQ5etyc5gaLsLkRf4l2qZxb1wd9a
	toCE6K9kZvVvjhHOevEOKuKRIbUq7D9emxdwV3nL5fMCgYAxrAvgG516mQW7VT85
	POotbYU/BMXtjiSFQOI+ocEDL7+ghUUJ82AL65+uksPIYDJmXhR83q+btLYgReNP
	7Q8PARod1LmXVAKXXrTu4T2ZD5jdpzWDbQPWyfIzV6pSAjk7LUN4c6CMp0s1w6pH
	PsXpXov/3HO/o5eSoqIkpTL8Vw==
	-----END PRIVATE KEY-----
